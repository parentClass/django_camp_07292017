"""hello_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from notes import views as view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', view.NoteList.as_view(), name='note_list'),
    url(r'^create/$', view.NoteCreate.as_view(), name='note_create'),
    url(r'^(?P<pk>\d+)/change/$', view.NoteUpdate.as_view(), name='note_update'),
    url(r'^(?P<pk>\d+)/delete/$', view.NoteDelete.as_view(), name='note_delete'),
]
