# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class SampleAbstract(models.Model):
	created_time = models.DateTimeField(auto_now_add=True)
	modified_time = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True


class Note(SampleAbstract):
	title = models.CharField(max_length=1024, blank=True)
	note = models.TextField(blank=True)
	
	def __str__(self):
		return self.title